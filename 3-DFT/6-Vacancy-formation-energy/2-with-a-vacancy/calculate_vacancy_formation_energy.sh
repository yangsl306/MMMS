#!/bin/bash
#
# Script to calculate the vacancy formation energy
#
# OUTCAR.02 are needed.
if [ ! -f "OUTCAR.02" ]; then
   echo "ERROR: File OUTCAR.02 not found, calculation incomplete!"
   exit
fi

# get number of atoms info
natom=`grep 'NIONS' OUTCAR.02|tail -1|awk '{print $12}'`

# get energy difference between ideal surface and bulk material
Ebulk=`grep 'free  energy' ../1-without-vacancy/OUTCAR|tail -1|awk '{print $5}'`
Etot=`grep 'free  energy' OUTCAR.02|tail -1|awk '{print $5}'`
Evf=`echo ${Etot} ${Ebulk} ${natom}|awk '{print $1 - $2*$3/($3+1)}'`

echo ""
echo "The vacancy formation energy is: ${Evf} eV."
echo ""
exit 0
