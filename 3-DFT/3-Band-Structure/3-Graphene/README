
The example in this directory illustrates the calculation of the 
band structure of graphene. Generally, the calculation is performed
with three steps:
1) SCF calculation with a small k-mesh;
2) non-SCF calculation with a dense k-mesh for DOS, but fixed charge density from step 1;
3) non-SCF calculation with line mode for band structure, the charge density is fixed as that from step 1.


To run this example, simply invoke:
   ./run
if you are using you own computer.  Or
   sbatch job.slurm
if you are using the Siyuan cluster.


Once the calculation is done, you should find some new files in your directory, such as:
  DOSCAR, EIGENVAL, ...

Invoke
   ../../0-tools/plot_graphene_bs, or
   plot_graphene_bs
to deduce the band structure and visualize it, or
   ../../0-tools/plot_graphene_dos, or
   plot_graphene_dos
to deduce the DOS and visualize it.

You can modify the corresponding files to carry out calculations for a different system;
the KPOINTS files do not need to be modified if the new system is also of honeycomb primitive structure.

