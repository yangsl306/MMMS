#!/bin/bash
#
# Script to extract and plot the band structure of copper from example 3
# You might need to modify this file if you have a different input/script
#
# Set environment
homedir=$( cd "$( dirname $0 )" && pwd)
. ${homedir}/environment

# Output from band calculation is a must
for ff in OUTCAR EIGENVAL DOSCAR
do
if [ ! -s "${ff}" ]; then
   echo "File: ${ff} not found or empty!"
   exit
fi
done

# Extract the band structure information by using vaspkit
cat > .vkit.in << EOF
21
211
EOF
${VKT} < .vkit.in > .vkit.out
# remove intermediate files
rm -rf .vkit.in .vkit.out KLINES.dat REFORMATTED_BAND.dat REFORMATTED_BAND_DW.dat BAND_GAP FERMI_ENERGY KLABELS REFORMATTED_BAND_UP.dat
#
# Script to draw the band structure
cat > .gnuplot.scr << EOF
set term post enha colo 20
set out 'Cu-BS.eps'

set xlabel ''
set ylabel 'Energy (eV)'

set xtics ("{/Symbol G}" 0, "X" 1.738, "W" 2.607, "K" 3.836, "{/Symbol G}" 5.341, "L" 7.185)
set ytics 5

set grid xtics lt 0

set xr [0:7.185]
set yr [-10:10]

unset key
plot 'BAND.dat' u 1:2 w l, 0 w l lt 2

# output a png file
set term png enha trans truec
set out "Cu-BS.png"
replot

EOF
# gnuplot is employed to draw the figure
gnuplot .gnuplot.scr;  rm -rf .gnuplot.scr

# visualize the figure by using evince
evince Cu-BS.eps

exit 0
