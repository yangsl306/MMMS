#!/bin/bash
#
# Script to extract and plot the electronic DOS of copper from example 3
# You might need to modify this file if you have a different input/script
#
# Set environment
homedir=$( cd "$( dirname $0 )" && pwd)
. ${homedir}/environment

# Output from band calculation is a must
for ff in DOSCAR PROCAR
do
if [ ! -s "${ff}" ]; then
   echo "File: ${ff} not found or empty!"
   exit
fi
done

# Extract the band structure information by using vaspkit
cat > .vkit.in << EOF
11
111
EOF
${VKT} < .vkit.in > .vkit.out
# remove intermediate files
rm -rf .vkit.in .vkit.out ITDOS.dat
#
# Script to draw the band structure
cat > .gnuplot.scr << EOF
set term post enha colo 20
set out 'Fe-DOS.eps'

set xlabel 'Energy (eV)'
set ylabel 'DOS'

# set grid xtics lt 0

set xr [-8:5]
set yr [-4:3]
set grid xtics

#unset key
plot 'TDOS.dat' u 1:2 w l t 'up', '' u 1:3 w l lt 1 lc 2 t 'down', 0 w l lt 0 t ''

# output a png file
set term png enha trans truec
set out "Fe-DOS.png"
replot

EOF
# gnuplot is employed to draw the figure
gnuplot .gnuplot.scr;  rm -rf .gnuplot.scr

# visualize the figure by using evince
evince Fe-DOS.eps
#
exit 0
