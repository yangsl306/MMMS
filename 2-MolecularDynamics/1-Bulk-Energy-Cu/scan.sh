#!/bin/bash
#
# Script to find the volume-energy relationship for FCC Cu.
#
# EAM potential (Cu_u6.eam) for Cu is required.


# Define the range of lattice constant to scan
alats=`seq 3.590 0.002 3.640`

# Header line of file to output the volume-energy info.
echo "# a (Ang) energy (eV)  volume (Ang^3)" > ev.dat

# Loop over lattice constant
for a in ${alats}
do
# create the input file
cat > in.lmp.alat << EOF
# 3d copper block
units        metal
boundary     p p p
atom_style   atomic

lattice      fcc $a
region       box block 0 10 0 10 0 10
create_box   1 box
create_atoms 1 box

# Potential
pair_style   eam
pair_coeff   * * Cu_u6.eam

neighbor     0.2 bin
neigh_modify delay 5

# Time step
timestep	0.001

# Variables to output
variable     vpa equal "vol / atoms"
variable     epa equal "pe  / atoms"

# MD 
fix          1 all nve
run		    0

# Output the energy
print        "${a} \${vpa} \${epa}" append ev.dat
EOF

# Run lammps
mpirun lmp -in in.lmp.alat
#
done

# Cleanup
rm -rf in.lmp.alat log.lammps
