#-----------------------------------------------------------------------------#
This example illustrates the determination of the equilibrium lattice constant
by scanning the volume-energy relationship for a given crystal at 0 K.

Files required/provided:
1) Cu_u6.eam
   EAM potential file for Cu.

2) in.lmp
   An example input file that functions the same as the script below to generate
   the energy-volume data.

3) scan.sh
   Script to perform the real calculation. It will generate lammps input files for
   a series of lattice parameters, and print the calculated per atom volume-energy
   data in "ev.dat".

4) README
   This file.

#-----------------------------------------------------------------------------#
Steps to run/analyze the data:
1) Calculate the energy of Cu at a series of volumes.
   If you are using your own computer to run this example, there are two ways to
   accomplish the task, you can use either of them:
     a) Modify scan.sh, if necessary. For example, if your lammps executable is named "lmp_mpi"
        instead of "lmp", you should replace 
           lmp -in in.lmp.alat
        by
           lmp_mpi -in in.lmp.alat
        in scan.sh.

     b) Run the script:
        bash ./scan.sh

   Or you can simply run lammps which has an embedded loop
      lmp -in in.lmp

   
   If you are using the Siyuan cluster, please submit your job to the queueing system:
      sbatch job.slurm

2) Once the calculation is done, the results should be written to "ev.dat". One can
   plot column 3 agains column 1. With gnuplot, it might reads:
     gnuplot
     gnuplot> plot 'ev.dat' u 1:3 w p pt 7

3) One can also use the tool provided in "../0-tools" to analyze the result:
     ../0-tools/eos_fit ev.dat 2 3

#-----------------------------------------------------------------------------#
(C) LT Kong   konglt(at)sjtu.edu.cn
