#-----------------------------------------------------------------------------#
This example illustrates the determination of the equilibrium lattice constant
by scanning the volume-energy relationship for a given crystal at 0 K.

Files required/provided:
1) Fe_mm.eam.fs
   EAM potential file for Fe.

2) in.lmp
   An example input file that helps to generate the energy-volume data.

3) README
   This file.

#-----------------------------------------------------------------------------#
Steps to run/analyze the data:
1) Run lammps simulation
      lmp -in in.lmp
   if you are using your own computer. Or
      sbatch job.slurm
   if you are using the Siyuan cluster.

2) Once the calculation is done, the results should be written to "ev.dat". One can
   plot column 3 agains column 1. With gnuplot, it might reads:
     gnuplot
     gnuplot> plot 'ev.dat' u 1:3 w p pt 7

3) One can also use the tool provided in "0-tools" to analyze the result:
     eos_fit ev.dat 2 3

#-----------------------------------------------------------------------------#
(C) LT Kong   konglt(at)sjtu.edu.cn
