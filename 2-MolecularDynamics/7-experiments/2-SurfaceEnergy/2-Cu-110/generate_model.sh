#!/bin/bash
#
# Script to generate the model by using atomsk (version b.0.11.2 expected.)
module load intel/19.0.5
ATOMSK="atomsk"
# ATOMSK="../../../0-tools/atomsk"

a0=3.615
# delete existing data.lmp
if [ -f "data.lmp" ]; then
   rm -rf data.lmp
fi

# use atomsk to generate the model
${ATOMSK} --create fcc ${a0} Cu orient [1-10] [001] [110] -duplicate 6 4 8 \
          -cell add 10 z -center com data.lmp
