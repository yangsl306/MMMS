#!/bin/bash
# Script to analyze the heating/cooling curve
#

# Extract info from log.lammps
awk /"^   Step "/,/"^Loop time"/ log.lammps > Info.dat

# Prepare the gnuplot script
cat > .plot.script << EOF
set term png
set out 'Cu-energy-temp.png'

set xlabel 'T (K)'
set ylabel 'E_P (eV)'
unset key
set xr [300:1800]
set yr [-7200:*]
set mxtics 5

plot 'Info.dat' u 2:4 w p pt 7 ps 0.2

set term post enha color 20
set out 'Cu-energy-temp.eps'
rep
EOF

# Draw the curve
gnuplot .plot.script
evince Cu-energy-temp.eps

# Cleanup
rm -rf .plot.script # Info.dat
