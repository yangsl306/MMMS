#!/bin/bash
#
# Script to deduce the critical stress and strain for dislocation to move
# It is simply the maximum stress

strain=0.
stress=0.
#
while IFS= read -r line
do
  gam=`echo ${line}|awk '{print $1}'`
  tau=`echo ${line}|awk '{print $2}'`
  if [ `echo "${tau} > ${stress}"|bc` -eq 1 ]; then
     stress=${tau}
     strain=${gam}
  fi
done < Info.dat
#
echo ""
echo "The critical strain is gamma  = ${strain}"
echo "The critical stress is tau_yz = ${stress} MPa."
echo ""
