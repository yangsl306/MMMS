#!/bin/bash
#
# Script to generate the model by using atomsk (version b.0.11.2 expected.)

# x: gliding direction,  [1-12]
# y: gliding plane norm, [-111]
# z: dislocation line direction, [110]
# Add 10 Angstrom of vacuum along x and y
module load intel/19.0.5
ATOMSK="atomsk"
# ATOMSK="../../../0-tools/atomsk"

a0=3.615
b=`echo ${a0}|awk '{print $1/2**0.5}'`

# delete existing data.lmp
if [ -f "data.lmp" ]; then
   rm -rf data.lmp
fi

# use atomsk to generate the model
${ATOMSK} --create fcc ${a0} Cu orient [1-12] [-111] [110] -duplicate 41 29 5 \
          -dislocation 0.51*box 0.501*box screw Z Y ${b} \
          -cell add 10 x -cell add 10 y -center com data.lmp
