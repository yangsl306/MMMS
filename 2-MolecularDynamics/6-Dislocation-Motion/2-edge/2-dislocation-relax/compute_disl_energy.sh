#!/bin/bash
#
# Script to deduce the dislocation energy per length.

# Extract the potential energy per atom for system without a dislocation
epa=`grep '^\^' ../1-without-dislocation/log.lammps|tail -1|awk '{print $9}'`

# Extract the potential energy and # of atoms for the mobile region with a dislocation
etot=`grep '^\^' log.lammps|head -1|awk '{print $9}'`
natm=`grep '^\^' log.lammps|head -1|awk '{print $12}'`

# Extract the dislocation length
dlen=`grep 'zlo zhi' data.lmp|awk '{print $2-$1}'`

# Now calculate the dislocation energy
E_disl=`echo $etot $natm $epa $dlen|awk '{print ($1 - $2*$3)/$4}'`

echo ""
echo "Total energy of the mobile region w disl: ${etot} eV."
echo "Total number of atoms within the region : ${natm}."
echo "The total length of the dislocation is  : ${dlen} Angstrom."
echo "The dislocation energy per length   is  : ${E_disl} eV/Angstrom."
echo ""
