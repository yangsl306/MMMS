#------------------------------------------------------------------------------#
This example generates a supercell of FCC Cu with orientations of [110] [-111] [1-12]
along x, y, and z, respectively. Fixed boundary condition along y, while periodic
along x and z. This is to server as a reference for evaluating the dislocation energy in 
the directory 2-dislocation-relax.

Files required/provided:
1) Cu_u6.eam
   EAM potential file for Cu.

2) in.lmp
   An example input file for lammps.

3) generate_model.sh
   A script to generate the atomic model by using atomsk.

4) job.slurm
   A script to submit jobs on Siyuan cluster

5) README
   This file.

#------------------------------------------------------------------------------#
Steps to run/analyze the data:
1) Generate the atomic model:
   On Siyuan cluster:
      ./generate_model.sh
   On your own computer:
      a) install atomsk;
      b) modify generate_model.sh and redefine ATOMSK as "atomsk";
      c) run: ./generate_model.sh

2) Run lammps:
   On Siyuan cluster:
      sbatch job.slurm

   On your own computer:
      lmp -in in.lmp
   or
      mpirun -np 4 lmp -in in.lmp -sf opt

3) Analyze the results:
   Once completed, check the last lines of log.lammps for information.
#------------------------------------------------------------------------------#
(C) LT Kong   konglt(at)sjtu.edu.cn
