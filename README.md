# MSE6701H data

- [《多尺度材料模拟与计算》课程材料](https://gitee.com/sjtu_konglt/MSE6701H)计算记录

- 输出及数据处理结果在对应目录下的 `data-*` 子目录中，仅供因任务提交后排队较久/代码运行较久暂无法得到输出文件的同学测试

- 切勿在该仓库的计算目录下重复提交任务，直接使用对应的 `data-*` 子目录中的输出文件进行后处理即可

- Copyright: (C) LT Kong konglt(at)sjtu.edu.cn Aug 2022
